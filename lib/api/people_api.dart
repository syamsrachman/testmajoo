import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:test_majoo/models/person.dart';

class PeopleApi {
  static Future<List<Person>> getAllPerson() async {
    var url = Uri.parse('https://swapi.dev/api/people/');
    var response = await http.get(url);
    var jsonObject = jsonDecode(response.body);

    List<Person> people = [];
    do {
      for (var mapPerson in jsonObject["results"]) {
        people.add(Person.fromMap(mapPerson));
      }
      response = await http.get(Uri.parse(jsonObject["next"]));
      jsonObject = jsonDecode(response.body);
    } while (jsonObject["next"] != null);
    return people;
  }
}
