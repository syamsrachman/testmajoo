import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/models/person.dart';
import 'package:test_majoo/sqflite/database.dart';

part 'favorite_event.dart';
part 'favorite_state.dart';

class FavoriteBloc extends Bloc<FavoriteEvent, FavoriteState> {
  FavoriteBloc() : super(FavoriteInitial()) {
    on<LoadFavorite>((event, emit) async {
      Database db = await new DB().database;
      List<Map> results = await db.rawQuery(
        "select b.* from favorite a join person b on a.person_id = b.id where a.user_id = " +
            event.userId,
      );

      List<Person> people = [];
      for (var map in results) {
        Map<String, dynamic> mapPerson = Map.from(map);
        mapPerson["is_favorite"] = true;
        people.add(Person.fromMap(mapPerson));
      }
      if (people.length > 0) {
        emit(FavoriteLoaded(people: people));
      } else {
        emit(FavoriteEmpty());
      }
    });
  }
}
