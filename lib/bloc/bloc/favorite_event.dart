part of 'favorite_bloc.dart';

@immutable
abstract class FavoriteEvent {}

class LoadFavorite extends FavoriteEvent {
  final String userId;
  LoadFavorite({required this.userId});
}
