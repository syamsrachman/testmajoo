part of 'favorite_bloc.dart';

@immutable
abstract class FavoriteState {}

class FavoriteInitial extends FavoriteState {}

class FavoriteLoaded extends FavoriteState {
  final List<Person> people;
  FavoriteLoaded({required this.people});
}

class FavoriteEmpty extends FavoriteState {}
