import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:test_majoo/models/user.dart';
import 'package:test_majoo/sqflite/database.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial()) {
    on<Login>((event, emit) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      Database db = await DB().database;
      List<Map<String, dynamic>> users = await db.query("users",
          limit: 1,
          where: "email = ? and password = ?",
          whereArgs: [event.email, event.password]);
      print(users.length);
      if (users.length > 0) {
        User user = User.fromMap(users.first);
        prefs.setString("user", jsonEncode(user.toMap()));
        print("login");
        emit(AlreadyLogin(user.id!, user.fullName, user.email));
      } else {
        emit(LoginError("Gagal"));
        print("gagal");
      }
    });

    on<Logout>((event, emit) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove("user");
      emit(NotLogin());
    });

    on<CheckStatus>((event, emit) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? user = prefs.getString("user");
      if (user != null) {
        Map<String, dynamic> json = jsonDecode(user);
        User objUser = User.fromMap(json);
        emit(AlreadyLogin(objUser.id!, objUser.fullName, objUser.email));
      } else {
        emit(NotLogin());
      }
    });
  }
}
