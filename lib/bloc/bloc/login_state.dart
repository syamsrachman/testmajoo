part of 'login_bloc.dart';

@immutable
abstract class LoginState {
  const LoginState();
}

class LoginInitial extends LoginState {}

class NotLogin extends LoginState {}

class AlreadyLogin extends LoginState {
  final int id;
  final String fullName;
  final String email;
  AlreadyLogin(this.id, this.fullName, this.email);
}

class LoginError extends LoginState {
  final String msg;

  LoginError(this.msg);
}
