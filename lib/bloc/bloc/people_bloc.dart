import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:test_majoo/models/person.dart';
import 'package:test_majoo/sqflite/database.dart';

part 'people_event.dart';
part 'people_state.dart';

class PeopleBloc extends Bloc<PeopleEvent, PeopleState> {
  PeopleBloc() : super(PeopleInitial()) {
    on<LoadPeople>((event, emit) async {
      Database db = await new DB().database;
      List<Map> results = await db.query("person",
          where: "name like ?",
          whereArgs: [event.name == null ? "%" : "%" + event.name! + "%"],
          orderBy: event.asc! ? "name asc" : "name desc");
      List<Person> people = [];
      for (var map in results) {
        Map<String, dynamic> mapPerson = Map.from(map);
        List<Map> listFav = await db.query("favorite",
            where: "user_id = ? and person_id = ?",
            whereArgs: [event.userId, mapPerson["id"]]);
        if (listFav.length > 0) {
          mapPerson["is_favorite"] = true;
        }
        people.add(Person.fromMap(mapPerson));
      }
      if (people.length > 0) {
        emit(PeopleLoaded(people));
      } else {
        emit(PeopleEmpty());
      }
    });
  }
}
