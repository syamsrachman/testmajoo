part of 'people_bloc.dart';

@immutable
abstract class PeopleEvent {}

class LoadPeople extends PeopleEvent {
  final String? name;
  final int userId;
  final bool? asc;
  LoadPeople(this.userId, {this.name, this.asc = true});
}

class EditPeople extends PeopleEvent {}

class DeletePeople extends PeopleEvent {}

class AddPeople extends PeopleEvent {}
