part of 'people_bloc.dart';

@immutable
abstract class PeopleState {}

class PeopleInitial extends PeopleState {}

class PeopleLoaded extends PeopleState {
  final List<Person> people;

  PeopleLoaded(this.people);
}

class PeopleEmpty extends PeopleState {}
