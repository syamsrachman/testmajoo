import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/api/people_api.dart';
import 'package:test_majoo/bloc/bloc/login_bloc.dart';
import 'package:test_majoo/bloc/bloc/people_bloc.dart';
import 'package:test_majoo/screen/login_screen.dart';
import 'package:test_majoo/screen/main_menu.dart';
import 'package:test_majoo/sqflite/database.dart';

import 'bloc/bloc/favorite_bloc.dart';
import 'models/person.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    _loadDB();
    return MultiBlocProvider(
      providers: [
        BlocProvider<LoginBloc>(create: (context) => LoginBloc()),
        BlocProvider<PeopleBloc>(create: (context) => PeopleBloc()),
        BlocProvider<FavoriteBloc>(create: (context) => FavoriteBloc()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            if (state is AlreadyLogin) {
              return MainMenu();
            } else if (state is NotLogin) {
              return LoginScreen();
            } else {
              context.read<LoginBloc>().add(CheckStatus());
              return Scaffold(
                body: Center(
                    child: CircularProgressIndicator(
                  color: Colors.green,
                )),
              );
            }
          },
        ),
        debugShowCheckedModeBanner: false,
      ),
    );
  }

  _loadDB() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLoaded = prefs.getBool('isLoaded') ?? false;
    if (isLoaded == false) {
      List<Person> people = await PeopleApi.getAllPerson();
      Database db = await new DB().database;

      for (var person in people) {
        Map<String, dynamic> map = person.toMap();
        map.remove("id");
        db.insert("person", map);
      }
      prefs.setBool("isLoaded", true);
    }
  }
}
