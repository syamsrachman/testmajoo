class Person {
  final int? id;
  final String name;
  final String height;
  final String mass;
  final String hairColor;
  final String skinColor;
  final String eyeColor;
  final String birthYear;
  final String gender;
  final String? url;
  final bool? isFavorite;

  Person(
      {this.id,
      required this.name,
      required this.height,
      required this.mass,
      required this.hairColor,
      required this.skinColor,
      required this.eyeColor,
      required this.birthYear,
      required this.gender,
      this.url,
      this.isFavorite = false});

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "name": name,
      "height": height,
      "mass": mass,
      "hair_color": hairColor,
      "skin_color": skinColor,
      "eye_color": eyeColor,
      "birth_year": birthYear,
      "gender": gender,
      "url": url,
    };
  }

  factory Person.fromMap(Map<String, dynamic> map) {
    return Person(
        id: map["id"],
        name: map["name"] ?? "",
        height: map["height"] ?? "",
        mass: map["mass"] ?? "",
        hairColor: map["hair_color"] ?? "",
        skinColor: map["skin_color"] ?? "",
        eyeColor: map["eye_color"] ?? "",
        birthYear: map["birth_year"] ?? "",
        gender: map["gender"] ?? "",
        url: map["url"] ?? "",
        isFavorite: map["is_favorite"] ?? false);
  }
}
