class User {
  final int? id;
  final String fullName;
  final String email;
  final String password;

  User(
      {required this.fullName,
      required this.email,
      required this.password,
      this.id});

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "full_name": fullName,
      "email": email,
      "password": password
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
        fullName: map["full_name"] ?? "",
        email: map["email"] ?? "",
        password: map["password"] ?? "",
        id: map["id"]);
  }
}
