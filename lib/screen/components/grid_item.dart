import 'package:flutter/material.dart';
import 'package:test_majoo/models/person.dart';

class GridItem extends StatelessWidget {
  final Person person;
  final Function onDelete;
  final Function onEdit;
  final Function onFav;
  final bool? isFavPage;
  const GridItem(
      {Key? key,
      required this.person,
      required this.onDelete,
      required this.onEdit,
      required this.onFav,
      this.isFavPage = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 80,
                  child: Text("Name"),
                ),
                Text(": "),
                Expanded(
                  child: Text(
                    person.name,
                    softWrap: true,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                SizedBox(
                  width: 80,
                  child: Text("Height"),
                ),
                Text(": "),
                Text(person.height),
              ],
            ),
            Row(
              children: [
                SizedBox(
                  width: 80,
                  child: Text("Mass"),
                ),
                Text(": "),
                Text(person.mass),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 80,
                  child: Text("Hair Color"),
                ),
                Text(": "),
                Expanded(
                    child: Text(
                  person.hairColor,
                  softWrap: true,
                  maxLines: 2,
                ))
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 80,
                  child: Text("Skin Color"),
                ),
                Text(": "),
                Expanded(
                  child: Text(
                    person.skinColor,
                    softWrap: true,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 80,
                  child: Text("Eye Color"),
                ),
                Text(": "),
                Expanded(
                  child: Text(
                    person.eyeColor,
                    softWrap: true,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 80,
                  child: Text("Birth Year"),
                ),
                Text(": "),
                Expanded(
                  child: Text(
                    person.birthYear,
                    softWrap: true,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 80,
                  child: Text("Gender"),
                ),
                Text(": "),
                Expanded(
                  child: Text(
                    person.gender,
                    softWrap: true,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: isFavPage!
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  onPressed: () {
                    onFav();
                  },
                  child: Icon(
                    Icons.favorite,
                    size: 16,
                    color: person.isFavorite! ? Colors.pink : Colors.white,
                  ),
                  style: ButtonStyle(
                    minimumSize: MaterialStateProperty.all(Size(30, 40)),
                  ),
                ),
                isFavPage!
                    ? Container()
                    : ElevatedButton(
                        onPressed: () {
                          onEdit();
                        },
                        child: Icon(
                          Icons.edit,
                          size: 16,
                        ),
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.yellow),
                            minimumSize:
                                MaterialStateProperty.all(Size(20, 40))),
                      ),
                isFavPage!
                    ? Container()
                    : ElevatedButton(
                        onPressed: () {
                          onDelete();
                        },
                        child: Icon(
                          Icons.delete,
                          size: 16,
                        ),
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.red),
                            minimumSize:
                                MaterialStateProperty.all(Size(20, 40))),
                      ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
