import 'package:flutter/material.dart';
import 'package:test_majoo/models/person.dart';

class ListItem extends StatelessWidget {
  final Person person;
  final Function onDelete;
  final Function onEdit;
  final Function onFav;
  const ListItem(
      {Key? key,
      required this.person,
      required this.onDelete,
      required this.onEdit,
      required this.onFav})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Card(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 7.5),
          padding: EdgeInsets.all(10),
          height: 170,
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 80,
                          child: Text("Name"),
                        ),
                        Text(": "),
                        Text(
                          person.name,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 80,
                          child: Text("Height"),
                        ),
                        Text(": "),
                        Text(person.height),
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 80,
                          child: Text("Mass"),
                        ),
                        Text(": "),
                        Text(person.mass),
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 80,
                          child: Text("Hair Color"),
                        ),
                        Text(": "),
                        Text(person.hairColor)
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 80,
                          child: Text("Skin Color"),
                        ),
                        Text(": "),
                        Expanded(
                          child: Text(
                            person.skinColor,
                            softWrap: true,
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 80,
                          child: Text("Eye Color"),
                        ),
                        Text(": "),
                        Expanded(
                          child: Text(
                            person.eyeColor,
                            softWrap: true,
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 80,
                          child: Text("Birth Year"),
                        ),
                        Text(": "),
                        Expanded(
                          child: Text(
                            person.birthYear,
                            softWrap: true,
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 80,
                          child: Text("Gender"),
                        ),
                        Text(": "),
                        Expanded(
                          child: Text(
                            person.gender,
                            softWrap: true,
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      Container(
        height: 120,
        margin: EdgeInsets.symmetric(horizontal: 2, vertical: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  onFav();
                },
                child: Container(
                  margin: EdgeInsets.only(top: 1),
                  decoration: BoxDecoration(color: Colors.green),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                    child: Icon(
                      Icons.favorite,
                      color: person.isFavorite! ? Colors.pink : Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  onEdit();
                },
                child: Container(
                  margin: EdgeInsets.only(top: 1),
                  decoration: BoxDecoration(color: Colors.yellow),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                    child: Icon(
                      Icons.edit,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Material(
              color: Colors.transparent,
              child: InkWell(
                splashColor: Colors.grey,
                onTap: () {
                  onDelete();
                },
                child: Container(
                  margin: EdgeInsets.only(top: 1, right: 1),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.only(topRight: Radius.circular(5)),
                      color: Colors.red),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                    child: Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    ]);
  }
}
