import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/bloc/bloc/favorite_bloc.dart';
import 'package:test_majoo/bloc/bloc/login_bloc.dart';
import 'package:test_majoo/bloc/bloc/people_bloc.dart';
import 'package:test_majoo/models/person.dart';
import 'package:test_majoo/sqflite/database.dart';

import 'components/grid_item.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favorite"),
      ),
      body: BlocBuilder<FavoriteBloc, FavoriteState>(
        builder: (context, state) {
          if (state is FavoriteEmpty) {
            return Center(
              child: Text("Data kosong"),
            );
          } else if (state is FavoriteLoaded) {
            List<Person> people = state.people;
            return GridView.builder(
                shrinkWrap: false,
                itemCount: people.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2, childAspectRatio: 0.5),
                itemBuilder: (context, index) {
                  return GridItem(
                    isFavPage: true,
                    person: people[index],
                    onDelete: () {},
                    onEdit: () {},
                    onFav: () async {
                      int userId =
                          (context.read<LoginBloc>().state as AlreadyLogin).id;
                      Database db = await new DB().database;
                      int results = await db.delete("favorite",
                          where: "user_id = ? and person_id = ?",
                          whereArgs: [userId, people[index].id]);
                      if (results == 0) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Gagal menghapus favorite")));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Berhasil menghapus favorite")));
                        context
                            .read<FavoriteBloc>()
                            .add(LoadFavorite(userId: userId.toString()));
                        context.read<PeopleBloc>().add(LoadPeople(userId));
                      }
                    },
                  );
                });
          } else if (state is FavoriteInitial) {
            String id =
                (context.read<LoginBloc>().state as AlreadyLogin).id.toString();
            context.read<FavoriteBloc>().add(LoadFavorite(userId: id));
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
