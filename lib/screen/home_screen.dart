import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/bloc/bloc/favorite_bloc.dart';
import 'package:test_majoo/bloc/bloc/login_bloc.dart';
import 'package:test_majoo/bloc/bloc/people_bloc.dart';
import 'package:test_majoo/models/person.dart';
import 'package:test_majoo/screen/components/grid_item.dart';
import 'package:test_majoo/screen/components/list_Item.dart';
import 'package:test_majoo/screen/person_form.dart';
import 'package:test_majoo/sqflite/database.dart';
import 'package:toggle_switch/toggle_switch.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ValueNotifier<bool> isList = new ValueNotifier(true);
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: SafeArea(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
            child: TextField(
              style: TextStyle(color: Colors.white),
              onChanged: (value) {
                int userId =
                    (context.read<LoginBloc>().state as AlreadyLogin).id;
                if (value.isEmpty) {
                  context.read<PeopleBloc>().add(LoadPeople(userId));
                } else {
                  context
                      .read<PeopleBloc>()
                      .add(LoadPeople(userId, name: value));
                }
              },
              decoration: InputDecoration(
                  hintText: "Search",
                  suffixIcon: Icon(Icons.search),
                  hintStyle: TextStyle(color: Colors.white)),
              autofocus: false,
            ),
          ),
        ),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ToggleSwitch(
                  minWidth: 45.0,
                  minHeight: 45,
                  cornerRadius: 10.0,
                  activeBgColors: [
                    [Colors.green],
                    [Colors.green]
                  ],
                  activeFgColor: Colors.white,
                  inactiveBgColor: Colors.grey,
                  inactiveFgColor: Colors.white,
                  totalSwitches: 2,
                  customIcons: [Icon(Icons.list), Icon(Icons.grid_on)],
                  onToggle: (index) {
                    if (index == 0) {
                      isList.value = true;
                    } else {
                      isList.value = false;
                    }
                  },
                ),
                ToggleSwitch(
                  minWidth: 80.0,
                  minHeight: 45,
                  cornerRadius: 10.0,
                  activeBgColors: [
                    [Colors.green],
                    [Colors.green]
                  ],
                  activeFgColor: Colors.white,
                  inactiveBgColor: Colors.grey,
                  inactiveFgColor: Colors.white,
                  totalSwitches: 2,
                  labels: ['Asc', 'Desc'],
                  onToggle: (index) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text("Fitur belum tersedia")));
                  },
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Expanded(
            child: ValueListenableBuilder(
              valueListenable: isList,
              builder: (context, value, child) {
                return BlocBuilder<PeopleBloc, PeopleState>(
                  builder: (context, state) {
                    if (state is PeopleLoaded) {
                      List<Person> people = state.people;
                      return value == true
                          ? ListView.builder(
                              itemCount: people.length,
                              itemBuilder: (context, index) {
                                return ListItem(
                                  onFav: () async {
                                    await fav(context, people[index]);
                                  },
                                  person: people[index],
                                  onEdit: () {
                                    edit(context, people[index]);
                                  },
                                  onDelete: () {
                                    delete(context, people[index].id!);
                                  },
                                );
                              })
                          : GridView.builder(
                              shrinkWrap: false,
                              itemCount: people.length,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2, childAspectRatio: 0.5),
                              itemBuilder: (context, index) {
                                return GridItem(
                                  isFavPage: false,
                                  onDelete: () {
                                    delete(context, people[index].id!);
                                  },
                                  onEdit: () {
                                    edit(context, people[index]);
                                  },
                                  onFav: () {
                                    fav(context, people[index]);
                                  },
                                  person: people[index],
                                );
                              });
                    } else if (state is PeopleEmpty) {
                      Center(
                        child: Text("Data kosong"),
                      );
                    } else if (state is PeopleInitial) {
                      int userId =
                          (context.read<LoginBloc>().state as AlreadyLogin).id;
                      context.read<PeopleBloc>().add(LoadPeople(userId));
                    }
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return PersonForm(title: "Add Person");
          }));
        },
        child: Icon(Icons.add),
      ),
    );
  }

  delete(BuildContext context, int id) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text("Yakin menghapus data?"),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Batal")),
              TextButton(
                  onPressed: () async {
                    Database db = await new DB().database;
                    int results = await db
                        .delete("person", where: "id = ?", whereArgs: [id]);
                    if (results == 0) {
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text("Gagal menghapus data")));
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text("Berhasil menghapus data")));
                      int userId =
                          (context.read<LoginBloc>().state as AlreadyLogin).id;
                      context.read<PeopleBloc>().add(LoadPeople(userId));
                      Navigator.pop(context);
                    }
                  },
                  child: Text("Oke")),
            ],
          );
        });
  }

  edit(BuildContext context, Person person) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return PersonForm(
        title: "Edit Person",
        person: person,
      );
    }));
  }

  fav(BuildContext context, Person person) async {
    String userId =
        (context.read<LoginBloc>().state as AlreadyLogin).id.toString();
    Database db = await new DB().database;
    int results = 0;
    if (person.isFavorite!) {
      results = await db.delete("favorite",
          where: "user_id = ? and person_id = ?",
          whereArgs: [userId, person.id]);
    } else {
      results = await db
          .insert("favorite", {"user_id": userId, "person_id": person.id});
    }
    context.read<FavoriteBloc>().add(LoadFavorite(userId: userId));
    if (results == 0) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(person.isFavorite!
              ? "Gagal menghapus favorite"
              : "Gagal menambah favorit")));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(person.isFavorite!
              ? "Berhasil menghapus favorite"
              : "Berhasil menyimpan favorit")));
      int userId = (context.read<LoginBloc>().state as AlreadyLogin).id;
      context.read<PeopleBloc>().add(LoadPeople(userId));
      context.read<FavoriteBloc>().add(LoadFavorite(userId: userId.toString()));
    }
  }
}
