import 'package:flutter/material.dart';
import 'package:test_majoo/screen/favorite_screen.dart';
import 'package:test_majoo/screen/home_screen.dart';
import 'package:test_majoo/screen/profile_screen.dart';

class MainMenu extends StatelessWidget {
  const MainMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> screens = [HomeScreen(), FavoriteScreen(), ProfileScreen()];
    ValueNotifier<int> index = new ValueNotifier(0);
    return Scaffold(
      body: ValueListenableBuilder(
        valueListenable: index,
        builder: (context, value, child) {
          return screens[value as int];
        },
      ),
      bottomNavigationBar: ValueListenableBuilder(
        valueListenable: index,
        builder: (context, value, child) {
          return BottomNavigationBar(
            onTap: (value) {
              index.value = value;
            },
            currentIndex: value as int,
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.favorite), label: "Favorite"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), label: "Profile")
            ],
          );
        },
      ),
    );
  }
}
