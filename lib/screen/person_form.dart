import 'package:flutter/material.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:test_majoo/bloc/bloc/login_bloc.dart';
import 'package:test_majoo/bloc/bloc/people_bloc.dart';
import 'package:test_majoo/models/person.dart';
import 'package:test_majoo/sqflite/database.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PersonForm extends StatelessWidget {
  final String title;
  final Person? person;
  const PersonForm({Key? key, required this.title, this.person})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = new TextEditingController();
    TextEditingController heightController = new TextEditingController();
    TextEditingController massController = new TextEditingController();
    TextEditingController hairController = new TextEditingController();
    TextEditingController skinController = new TextEditingController();
    TextEditingController eyeController = new TextEditingController();
    TextEditingController birthController = new TextEditingController();
    TextEditingController genderController = new TextEditingController();
    if (title == "Edit Person" && person != null) {
      nameController.text = person!.name;
      heightController.text = person!.height;
      massController.text = person!.mass;
      hairController.text = person!.hairColor;
      skinController.text = person!.skinColor;
      eyeController.text = person!.eyeColor;
      birthController.text = person!.birthYear;
      genderController.text = person!.gender;
    }
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              TextField(
                decoration: InputDecoration(labelText: "Name"),
                controller: nameController,
              ),
              TextField(
                decoration: InputDecoration(labelText: "Height"),
                controller: heightController,
              ),
              TextField(
                decoration: InputDecoration(labelText: "Mass"),
                controller: massController,
              ),
              TextField(
                decoration: InputDecoration(labelText: "Hair Color"),
                controller: hairController,
              ),
              TextField(
                decoration: InputDecoration(labelText: "Skin Color"),
                controller: skinController,
              ),
              TextField(
                decoration: InputDecoration(labelText: "Eye Color"),
                controller: eyeController,
              ),
              TextField(
                decoration: InputDecoration(labelText: "Birth Year"),
                controller: birthController,
              ),
              TextField(
                decoration: InputDecoration(labelText: "Gender"),
                controller: genderController,
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () async {
                  Database db = await new DB().database;
                  Person person = new Person(
                      name: nameController.text,
                      height: heightController.text,
                      mass: massController.text,
                      hairColor: hairController.text,
                      skinColor: skinController.text,
                      eyeColor: eyeController.text,
                      birthYear: birthController.text,
                      gender: genderController.text);
                  int results = 0;
                  Map<String, dynamic> map = person.toMap();
                  map.remove("id");
                  if (title == "Edit Person") {
                    results = await db.update("person", map,
                        where: "id = ?", whereArgs: [this.person!.id]);
                  } else {
                    results = await db.insert("person", map);
                  }
                  if (results == 0) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        new SnackBar(content: Text("Gagal Menyimpan")));
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                        new SnackBar(content: Text("Berhasil Menyimpan")));
                    int userId =
                        (context.read<LoginBloc>().state as AlreadyLogin).id;
                    context.read<PeopleBloc>().add(LoadPeople(userId));
                    Navigator.pop(context);
                  }
                },
                child: Text("Save"),
                style: ButtonStyle(
                    minimumSize: MaterialStateProperty.all(Size(100, 50))),
              )
            ],
          ),
        ),
      ),
    );
  }
}
