import 'package:flutter/material.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:test_majoo/sqflite/database.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = new TextEditingController();
    TextEditingController emailController = new TextEditingController();
    TextEditingController passwordController = new TextEditingController();
    TextEditingController rePasswordController = new TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: Text("Register"),
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(labelText: "Full Name"),
              controller: nameController,
            ),
            TextField(
              decoration: InputDecoration(labelText: "Email"),
              controller: emailController,
            ),
            TextField(
              obscureText: true,
              decoration: InputDecoration(labelText: "Password"),
              controller: passwordController,
            ),
            TextField(
              obscureText: true,
              decoration: InputDecoration(labelText: "Re-Type Password"),
              controller: rePasswordController,
            ),
            SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () async {
                if (nameController.text.isNotEmpty &&
                    emailController.text.isNotEmpty &&
                    passwordController.text.isNotEmpty &&
                    rePasswordController.text.isNotEmpty) {
                  Database db = await DB().database;
                  List<Map> listUser = await db.query("users",
                      where: "email = ?", whereArgs: [emailController.text]);
                  if (listUser.length > 0) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        new SnackBar(content: Text("Email sudah digunakan")));
                  } else {
                    db.insert("users", {
                      "full_name": nameController.text,
                      "email": emailController.text,
                      "password": passwordController.text
                    });

                    ScaffoldMessenger.of(context).showSnackBar(
                        new SnackBar(content: Text("Registrasi sukses")));
                    Navigator.pop(context);
                  }
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
                      content: Text("Terdapat input yang kosong")));
                }
              },
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Text("Register"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
