import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DB {
  Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database!;
  }

  initDB() async {
    String path = join(await getDatabasesPath(), "tes.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE users ("
          "id INTEGER PRIMARY KEY AUTOINCREMENT,"
          "full_name TEXT,"
          "email TEXT,"
          "password TEXT"
          ")");
      await db.execute("CREATE TABLE person ("
          "id INTEGER PRIMARY KEY AUTOINCREMENT,"
          "name TEXT,"
          "height TEXT,"
          "mass TEXT,"
          "hair_color TEXT,"
          "skin_color TEXT,"
          "eye_color TEXT,"
          "birth_year TEXT,"
          "gender TEXT,"
          "url TEXT"
          ")");
      await db.execute("CREATE TABLE favorite ("
          "id INTEGER PRIMARY KEY AUTOINCREMENT,"
          "user_id INTEGER,"
          "person_id INTEGER"
          ")");
    });
  }
}
